/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.samples.commands.basic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;

import java.io.*;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.FILE;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 *<pre>
	This is package to read text file and store content and return it as a string variable
 * </pre>
 * 
 * @author Takehiko Isono
 */

//BotCommand makes a class eligible for being considered as an action.
@BotCommand

//CommandPks adds required information to be dispalable on GUI.
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "ReadTextIntoString", label = "[[readtext2string.label]]",
		node_label = "[[readtext2string.node_label]]", description = "[[readtext2string.description]]", icon = "pkg.svg",
		
		//Return type information. return_type ensures only the right kind of variable is provided on the UI. 
		return_label = "[[readtext2string.return_label]]", return_type = STRING, return_required = true)
public class ReadText2String {
	
	//Messages read from full qualified property file name and provide i18n capability.
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public Value<String> action(
			//Idx 1 would be displayed first, with a text box for entering the value.
			@Idx(index = "1", type = AttributeType.FILE)
			//UI labels.
			@Pkg(label = "[[readtext2string.firstString.label]]")
			//Ensure that a validation error is thrown when the value is null.
			@NotEmpty 
			String inputfile,

			//ファイルの言語コード
			@Idx(index = "2", type = AttributeType.SELECT, options = {
					@Idx.Option(index = "2.1", pkg = @Pkg(label = "[[Characode.option1.label]]", value = "US-ASCII")),
					@Idx.Option(index = "2.2", pkg = @Pkg(label = "[[Characode.option2.label]]", value = "Shift_JIS")),
					@Idx.Option(index = "2.3", pkg = @Pkg(label = "[[Characode.option3.label]]", value = "UTF-8")),
					@Idx.Option(index = "2.4", pkg = @Pkg(label = "[[Characode.option4.label]]", value = "EUC-JP"))
			})
			@Pkg(label = "[[Characode.option.label]]")
			@NotEmpty
					String sCharaCode

			) {
		
		//Internal validation, to disallow empty strings. No null check needed as we have NotEmpty on firstString.
		if ("".equals(inputfile.trim()))
			throw new BotCommandException(MESSAGES.getString("emptyInputString", "firstString"));


		//Business logic
			//Variable to hold result
			String result = "";
			//Open file
			//Read contents
			try{
				File file = new File(inputfile);
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inputfile), sCharaCode));

				//Read first line.
				String str;
				while((str = br.readLine()) != null){
					if (result.isBlank()){
						result = str;
					}else
					{
						result = result + "\n" + str;
					}
				}

				br.close();
			}catch(FileNotFoundException e){
				System.out.println(e);
			}catch(IOException e){
				System.out.println(e);
			}

		//Return StringValue.
		return new StringValue(result);
	}
}
