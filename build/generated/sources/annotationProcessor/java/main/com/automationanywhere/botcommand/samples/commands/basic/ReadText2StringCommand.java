package com.automationanywhere.botcommand.samples.commands.basic;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ReadText2StringCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(ReadText2StringCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    ReadText2String command = new ReadText2String();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("inputfile") && parameters.get("inputfile") != null && parameters.get("inputfile").get() != null) {
      convertedParameters.put("inputfile", parameters.get("inputfile").get());
      if(convertedParameters.get("inputfile") !=null && !(convertedParameters.get("inputfile") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","inputfile", "String", parameters.get("inputfile").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("inputfile") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","inputfile"));
    }

    if(parameters.containsKey("sCharaCode") && parameters.get("sCharaCode") != null && parameters.get("sCharaCode").get() != null) {
      convertedParameters.put("sCharaCode", parameters.get("sCharaCode").get());
      if(convertedParameters.get("sCharaCode") !=null && !(convertedParameters.get("sCharaCode") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sCharaCode", "String", parameters.get("sCharaCode").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sCharaCode") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sCharaCode"));
    }
    if(convertedParameters.get("sCharaCode") != null) {
      switch((String)convertedParameters.get("sCharaCode")) {
        case "US-ASCII" : {

        } break;
        case "Shift_JIS" : {

        } break;
        case "UTF-8" : {

        } break;
        case "EUC-JP" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","sCharaCode"));
      }
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("inputfile"),(String)convertedParameters.get("sCharaCode")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
